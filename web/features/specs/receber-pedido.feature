#encoding: UTF-8

Feature: Receber pedido
    Sendo um anúnciante que possui equipamentos cadastrados
    Desejo receber pedidos de locação
    Para que eu possa decidir se quero aprova-los ou rejeita-los

    @recebe_pedido
    Scenario: Receber pedido
        Given que meu perfil de anunciante é "luiz@anunciante.com" e "123456"
        And que eu tenho o seguinte equipamento cadastrado:
            | thumb     | trompete.jpg |
            | nome      | Trompete     |
            | categoria | Outros       |
            | preco     | 100          |
        And acesso o meu dashboard
        When "maria@locataria.com" e "123456" solicita a locacao desse equipo
        Then devo ver a seguinte mensagem:
            """
            maria@locataria.com deseja alugar o equipamento: Trompete em: DATA_ATUAL
            """
        And devo ver os links: "ACEITAR" e "REJEITAR" no pedido
