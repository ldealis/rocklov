#encoding: UTF-8

Feature: Login
    Sendo um usuário cadastrado
    Quero acessar o sistema da rocklov
    Para que eu possa anunciar meus equipamentos musicais

    @login
    Scenario: Login do usuário
        Given que acesso a página principal
        When submeto minhas credenciais com "dealis.luiz@gmail.com" e "123456"
        Then sou redirecionado para o dashboard

    Scenario Outline: Tentar logar
        Given que acesso a página principal
        When submeto minhas credenciais com "<email_input>" e "<senha_input>"

        Then vejo a mensagem de alerta: "<mensagem_output>"

        Examples:
            | email_input             | senha_input | mensagem_output                  |
            | dealis.luiz@gmail.com   | 1234567     | Usuário e/ou senha inválidos.    |
            | dealis98.luiz@gmail.com | 123456      | Usuário e/ou senha inválidos.    |
            | dealis.luiz%gmail.com   | 123456      | Oops. Informe um email válido!   |
            |                         | 123456      | Oops. Informe um email válido!   |
            | dealis.luiz@gmail.com   |             | Oops. Informe sua senha secreta! |


# Scenario: Senha incorreta
#     Given que acesso a página principal
#     When submeto minhas credenciais com senha incorreta
#     Then vejo a mensagem de alerta "Usuário e/ou senha inválidos."

# Scenario: Email não cadastrado
#     Given que acesso a página principal
#     When submeto minhas credenciais com email que não existe na rocklov
#     Then vejo a mensagem de alerta "Usuário e/ou senha inválidos."

# Scenario: Email incorreto
#     Given que acesso a página principal
#     When submeto minhas credenciais com email incorreto
#     Then vejo a mensagem de alerta "Oops. Informe um email válido!"

# Scenario: Email não informado
#     Given que acesso a página principal
#     When submeto minhas credenciais com email vazio
#     Then vejo a mensagem de alerta "Usuário e/ou senha inválidos."

# Scenario: Senha não informada
#     Given que acesso a página principal
#     When submeto minhas credenciais sem preencher o campo senha
#     Then vejo a mensagem de alerta "Oops. Informe sua senha secreta!"