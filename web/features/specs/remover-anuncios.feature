
Feature: Remover anúncios
    Sendo um anunciante que possui um equipamento indesejado
    Quero poder remover esse anúncio
    Para que eu possa manter o Dashboard atualizado

    Background: Login
        * Login com "luiz4@gmail.com" e "123456"

    @remove_anuncio
    Scenario: Remover um anuncio
        Given que tenho um anuncio indesejado:
            | thumbnail | telecaster.jpg |
            | nome      | Telecaster     |
            | categoria | Outros         |
            | preco     | 273            |
        When solicito a exclusao desse item
        And confirmo o exclusao
        Then nao devo ver esse item no meu Dashboard

    @remove_anuncio_canc
    Scenario: Desistir da exclusao
        Given que tenho um anuncio indesejado:
            | thumbnail | conga.jpg |
            | nome      | Conga     |
            | categoria | Outros         |
            | preco     | 114            |
        When solicito a exclusao desse item
        And nao confirmo a exclusao
        Then este item deve permanecer no meu Dashboard