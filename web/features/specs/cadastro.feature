#encoding: UTF-8

Feature: Cadastro
    Sendo um musico que possui equipamentos musicais
    Quero fazer o meu cadastro no rocklov
    Para que possa diponibiliza-los para alocação

    @cadastro_sucesso
    Scenario: Realizar cadastro
        Given que acesso a página de cadastro
        When submeto o seguinte formulário de cadastro:
            | nome | email          | senha  |
            | Luiz | luiz@gmail.com | 123456 |
        Then sou redirecionado para o dashboard

    Scenario Outline: Tentativa de cadastro
        Given que acesso a página de cadastro
        When submeto o seguinte formulário de cadastro:
            | nome         | email         | senha         |
            | <nome_input> | <email_input> | <senha_input> |
        Then vejo a mensagem de alerta: "<mensagem_output>"

        Examples:
            | nome_input | email_input    | senha_input | mensagem_output                  |
            |            | luiz@gmail.com | 123456      | Oops. Informe seu nome completo! |
            | Luiz       |                | 123456      | Oops. Informe um email válido!   |
            | Luiz       | luiz&gmail.com | 123456      | Oops. Informe um email válido!   |
            | Luiz       | luiz@gmail.com |             | Oops. Informe sua senha secreta! |

# @cadastro_falha
# Cenario: Submeter cadastro sem nome
#     Dado que acesso a página de cadastro
#     Quando submeto o seguinte formulário de cadastro:
#         |nome|email         |senha |
#         |    |luiz@gmail.com|123456|
#     Então vejo a mensagem de alerta: "Oops. Informe seu nome completo!"

# @cadastro_falha
# Cenario: Submeter cadastro sem email
#     Dado que acesso a página de cadastro
#     Quando submeto o seguinte formulário de cadastro:
#         |nome|email         |senha |
#         |Luiz|              |123456|
#     Então vejo a mensagem de alerta: "Oops. Informe um email válido!"

# @cadastro_falha
# Cenario: Submeter cadastro com email inválido
#     Dado que acesso a página de cadastro
#     Quando submeto o seguinte formulário de cadastro:
#         |nome|email         |senha |
#         |Luiz|luiz&gmail.com|123456|
#     Então vejo a mensagem de alerta: "Oops. Informe um email válido!"

# @cadastro_falha
# Cenario: Submeter o cadastro sem a senha
#     Dado que acesso a página de cadastro
#     Quando submeto o seguinte formulário de cadastro:
#         |nome|email         |senha |
#         |Luiz|luiz@gmail.com|      |
#     Então vejo a mensagem de alerta: "Oops. Informe sua senha secreta!"