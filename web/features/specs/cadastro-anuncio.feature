#encoding: UTF-8

Feature: Cadastro de Anúncios

    Sendo usuário cadastrado no RockLov que possui equipamentos músicais
    Quero cadastrar meus equipamentos
    Para que eu possa disponibiliza-los para locação

    Background: Login
        * Login com "luiz10@gmail.com" e "123456"

    Scenario: Novo equipo
        # Given que estou logado como "luiz10@gmail.com" e "123456"
        Given que acesso o formulario de cadastro de anuncios
        And que eu tenha o seguinte equipamento:
            | thumbnail | fender-sb.jpg |
            | nome      | Fender Strato |
            | categoria | Cordas        |
            | preco     | 200           |

        When submeto o cadastro desse item
        Then devo ver esse item no meu Dashboard

    @cad_sem_foto
    Scenario Outline: Tentativa de cadastro de Anúncios
        Given que acesso o formulario de cadastro de anuncios
        And que eu tenha o seguinte equipamento:
            | thumbnail | <foto_input>      |
            | nome      | <nome_input>      |
            | categoria | <categoria_input> |
            | preco     | <preco_input>     |

        When submeto o cadastro desse item
        Then deve conter a mensagem de alerta: "<saida_output>"

        Examples:
            | foto_input    | nome_input   | categoria_input | preco_input | saida_output                         |
            |               | Violão Nylon | Cordas          | 180         | Adicione uma foto no seu anúncio!    |
            | clarinete.jpg |              | Outros          | 230         | Informe a descrição do anúncio!      |
            | clarinete.jpg | Clarinete    |                 | 215         | Informe a categoria                  |
            | clarinete.jpg | Clarinete    | Outros          |             | Informe o valor da diária            |
            | clarinete.jpg | Clarinete    | Outros          | abcdce      | O valor da diária deve ser numérico! |
            | clarinete.jpg | Clarinete    | Outros          | abcd1       | O valor da diária deve ser numérico! |
            | clarinete.jpg | Clarinete    | Outros          | 100e        | O valor da diária deve ser numérico! |