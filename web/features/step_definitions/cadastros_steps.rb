Given('que acesso a página de cadastro') do
    @signup_page.open   
end

When('submeto o seguinte formulário de cadastro:') do |table|
    user = table.hashes.first
    MongoDB.new.remove_user(user[:email])      
    @signup_page.create(user)

    # find("#fullName").set user[:nome]
    # find("#email").set user[:email]
    # find("#password").set user[:senha]
    # click_button "Cadastrar"
end

