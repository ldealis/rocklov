Given("Login com {string} e {string}") do |email, senha|
  @email = email
  @login_page.open
  @login_page.with(email, senha)
end

Given("que acesso o formulario de cadastro de anuncios") do
  @dash_page.goto_equipo_form
  # click_button "Criar anúncio"
  # #checkpoint para validar se estou na tela correta
  # expect(page).to have_css "#equipoForm"
end

Given("que eu tenha o seguinte equipamento:") do |table|
  @anuncio = table.rows_hash
  MongoDB.new.remove_equipo(@anuncio[:nome], @email)
end

When("submeto o cadastro desse item") do
  @equipos_page.create(@anuncio)
end

Then("devo ver esse item no meu Dashboard") do
  # anuncios = find(".equipo-list")
  expect(@dash_page.equipo_list).to have_content @anuncio[:nome]
  expect(@dash_page.equipo_list).to have_content "R$#{@anuncio[:preco]}/dia"
end

Then("deve conter a mensagem de alerta: {string}") do |expect_alert|
  expect(@alert.dark).to have_text expect_alert
end

# Remover anuncios
Given("que tenho um anuncio indesejado:") do |table|
  @user_id = page.execute_script("return localStorage.getItem('user')")
  thumbnail = File.open(File.join(Dir.pwd, "features/support/fixtures/images", table.rows_hash[:thumbnail]), "rb")
  @equipo = {
    thumbnail: thumbnail,
    name: table.rows_hash[:nome],
    category: table.rows_hash[:categoria],
    price: table.rows_hash[:preco],
  }
  EquiposService.new.create(@equipo, @user_id)
  sleep 1 #think time
  visit current_path
end

When("solicito a exclusao desse item") do
  @dash_page.request_removal(@equipo[:name])
end

When("confirmo o exclusao") do
  @dash_page.confirm_removal
end

Then("nao devo ver esse item no meu Dashboard") do
  expect(@dash_page.has_no_equipo?(@equipo[:name])).to be true
end

# Cancelar remoção de anúncios
When("nao confirmo a exclusao") do
  @dash_page.cancel_removal
end

Then("este item deve permanecer no meu Dashboard") do
  expect(@dash_page.equipo_list).to have_content @equipo[:name]
end
