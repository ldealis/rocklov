Given("que meu perfil de anunciante é {string} e {string}") do |email, senha|
  @email_anunciante = email
  @senha_anunciante = senha
end

Given("que eu tenho o seguinte equipamento cadastrado:") do |table|
  pending # Write code here that turns the phrase above into concrete actions
  @user_id = SessionsService.new.get_user_id(@email_anunciante, @senha_anunciante)
  thumbnail = File.open(File.join(Dir.pwd, "features/support/fixtures/images", table.rows_hash[:thumb]), "rb")
  @equipo = {
    thumbnail: thumbnail,
    name: table.rows_hash[:nome],
    category: table.rows_hash[:categoria],
    price: table.rows_hash[:preco],
  }
  MongoDB.new.remove_equipo(@equipo[:name], @email_anunciante)
  result = EquiposService.new.create(@equipo, @user_id)
  @equipo_id = result.parsed_response["_id"]
  log @equipo_id
end

Given("acesso o meu dashboard") do
  @login_page.open
  @login_page.with(@email_anunciante, @senha_anunciante)
  expect(@dash_page.on_dash?).to be true
  sleep 1
end

When("{string} e {string} solicita a locacao desse equipo") do |string, string2|
  pending # Write code here that turns the phrase above into concrete actions
end

Then("devo ver a seguinte mensagem:") do |doc_string|
  pending # Write code here that turns the phrase above into concrete actions
end

Then("devo ver os links: {string} e {string} no pedido") do |string, string2|
  pending # Write code here that turns the phrase above into concrete actions
end
