#utilizado no login e no cadastro que auto loga o usuário
Then('sou redirecionado para o dashboard') do
    # expect(page).to have_css ".dashboard"
    expect(@dash_page.on_dash?).to be true
end

#utilizado no form do login e cadastro
Then('vejo a mensagem de alerta: {string}') do |expect_alert|    
    expect(@alert.dark).to eql expect_alert
end