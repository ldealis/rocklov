require_relative "routes/signup"
require_relative "routes/sessions"
require_relative "routes/equipos"
require_relative "libs/mongo"
require_relative "helpers"

require "digest/md5"

def to_md5(pass)
  return Digest::MD5.hexdigest(pass)
end

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

  config.before(:suite) do
    users = [
      { nome: "Luiz Dealis", email: "dealis.luiz@gmail.com", password: to_md5("123456") },
      { nome: "Luiz Dashboard", email: "luiz.dash@gmail.com", password: to_md5("123456") },
      { nome: "Luiz 10", email: "luiz10@gmail.com", password: to_md5("123456") },
      { nome: "Luiz 2", email: "luiz2@gmail.com", password: to_md5("123456") },
      { nome: "Luiz 3", email: "luiz3@gmail.com", password: to_md5("123456") },
      { nome: "Robson Dealis", email: "robson@gmail.com", password: to_md5("123456") },
    ]

    MongoDB.new.drop_danger
    MongoDB.new.insert_users(users)
  end
end
