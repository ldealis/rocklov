describe "POST /equipos" do
  before(:all) do
    payload = { email: "luiz.dash@gmail.com", password: "123456" }
    result = Sessions.new.login(payload)
    # puts result
    @user_id = result.parsed_response["_id"]
    # puts @user_id
  end
  context "Novo equipo" do
    before(:all) do
      # thumbnail = File.open(File.join(Dir.pwd, "spec/fixtures/images", "kramer.jpg"), "rb")
      payload = {
        thumbnail: Helpers::get_thumb("kramer.jpg"),
        name: "Kramer Eddie Van Halen",
        category: "Cordas",
        price: 150,
      }
      MongoDB.new.remove_equipo(payload[:name], @user_id)
      @result = Equipos.new.create(payload, @user_id)
      puts @result
    end

    it "validar code equipo" do
      expect(@result.code).to eql 200
    end
  end
  context "Nao autorizado" do
    before(:all) do
      # thumbnail = File.open(File.join(Dir.pwd, "spec/fixtures/images", "baixo.jpg"), "rb")

      payload = {
        thumbnail: Helpers::get_thumb("baixo.jpg"),
        name: "Contra baixo",
        category: "Cordas",
        price: 89,
      }
      @result = Equipos.new.create(payload, nil)
    end
    it "validar code 401" do
      expect(@result.code).to eql 401
    end
  end
end
