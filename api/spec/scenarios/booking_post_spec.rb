describe "POST /equipos/{equipo_id}/bookings" do
  before(:all) do
    payload = { email: "luiz3@gmail.com", password: "123456" }
    result = Sessions.new.login(payload)
    @luiz3_id = result.parsed_response["_id"]
  end
  context "Solicitar locacao" do
    before(:all) do
      #Dado que pessoa 1 tem 1 equipamento para locação
      result = Sessions.new.login({ email: "luiz2@gmail.com", password: "123456" })
      luiz2_id = result.parsed_response["_id"]

      fender = {
        thumbnail: Helpers::get_thumb("fender-sb.jpg"),
        name: "Fender Strato",
        category: "Cordas",
        price: 180,
      }
      MongoDB.new.remove_equipo(fender[:name], luiz2_id)
      result = Equipos.new.create(fender, luiz2_id)
      fender_id = result.parsed_response["_id"]

      @result = Equipos.new.bookings(fender_id, @luiz3_id)
    end

    it "Validar code 200" do
      expect(@result.code).to eql 200
    end
  end
end
