describe "POST /sessions" do
  context "login com sucesso" do
    before(:all) do
      payload = {
        email: "luiz10@gmail.com",
        password: "123456",
      }
      @result = Sessions.new.login(payload)
    end
    it "valida status code" do
      expect(@result.code).to eql 200
    end
    it "valida hash id" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  #   examples = [

  #     {
  #       title: "email em branco",
  #       payload: { email: "", password: "123456" },
  #       code: 412,
  #       error: "required email",
  #     },
  #     {
  #       title: "sem campo email",
  #       payload: { password: "123456" },
  #       code: 412,
  #       error: "required email",
  #     },
  #     {
  #       title: "senha inválida",
  #       payload: { email: "luiz10@gmail.com", password: "1234567" },
  #       code: 401,
  #       error: "Unauthorized",
  #     },
  #     {
  #       title: "senha em branco",
  #       payload: { email: "luiz10@gmail.com", password: "" },
  #       code: 412,
  #       error: "required password",
  #     },
  #     {
  #       title: "sem campo senha",
  #       payload: { email: "luiz10@gmail.com" },
  #       code: 412,
  #       error: "required password",
  #     },
  #     {
  #       title: "usuário não existe",
  #       payload: { email: "luiz100@gmail.com", password: "123456" },
  #       code: 401,
  #       error: "Unauthorized",
  #     },
  #   ]

  examples = Helpers::get_fixture("login")

  examples.each do |example|
    context "#{example[:title]}" do
      before(:all) do
        @result = Sessions.new.login(example[:payload])
      end
      it "valida status code #{example[:code]}" do
        expect(@result.code).to eql example[:code]
      end
      it "valida mensagem de erro" do
        expect(@result.parsed_response["error"]).to eql example[:error]
      end
    end
  end
end
