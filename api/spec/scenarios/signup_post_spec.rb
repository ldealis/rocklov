describe "POST /signup" do
  context "cadastro com sucesso" do
    before(:all) do
      payload = {
        name: "Luiz 10",
        email: "luiz10@gmail.com",
        password: "123456",
      }
      MongoDB.new.remove_user(payload[:email])
      @result = Signup.new.create(payload)
    end
    it "valida status code" do
      expect(@result.code).to eql 200
    end
    it "valida hash id" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  context "Usuario duplicado" do
    before(:all) do
      # cadastro um novo usuario, cadastro o mesmo usuario novamente, analiso o retorno
      payload = { name: "Luiz Duplic", email: "luiz.duplic@gmail.com", password: "123456" }
      # removo usuário caso exista para ter certeza que posso criar o usuario do teste
      MongoDB.new.remove_user(payload)
      Signup.new.create(payload)
      # usuario criado, agora tento cadastrar novamente o mesmo
      @result = Signup.new.create(payload)
    end
    it "Valida status code" do
      expect(@result.code).to eql 409
    end
    it "Valida mensagem de erro" do
      expect(@result.parsed_response["error"]).to eql "Email already exists :("
    end
    it "Valida code regra de negocio" do
      expect(@result.parsed_response["code"]).to eql 1001
    end
  end

  examples = [
    {
      title: "Nome vazio",
      payload: { name: "", email: "luiz10@gmail.com", password: "123456" },
      code: 412,
      error: "required name",
    },
    {
      title: "Sem nome",
      payload: { email: "luiz10@gmail.com", password: "123456" },
      code: 412,
      error: "required name",
    },
    {
      title: "Email vazio",
      payload: { name: "Luiz", email: "", password: "123456" },
      code: 412,
      error: "required email",
    },
    {
      title: "Sem email",
      payload: { name: "Luiz", password: "123456" },
      code: 412,
      error: "required email",
    },
    {
      title: "Email formato errado",
      payload: { name: "Luiz", email: "luiz10&gmail.com", password: "123456" },
      code: 412,
      error: "wrong email",
    },
    {
      title: "Senha vazia",
      payload: { name: "Luiz", email: "luiz10@gmail.com", password: "" },
      code: 412,
      error: "required password",
    },
    {
      title: "Sem senha",
      payload: { name: "Luiz", email: "luiz10@gmail.com" },
      code: 412,
      error: "required password",
    },
  ]

  examples.each do |example|
    context "#{example[:title]}" do
      before(:all) do
        @result = Signup.new.create(example[:payload])
      end
      it "valida status code" do
        expect(@result.code).to eql example[:code]
      end
      it "valida mensagem de erro" do
        expect(@result.parsed_response["error"]).to eql example[:error]
      end
    end
  end
end
