describe "GET /equipos/{equipo_id}" do
  before(:all) do
    payload = { email: "luiz.dash@gmail.com", password: "123456" }
    result = Sessions.new.login(payload)
    @user_id = result.parsed_response["_id"]
  end
  context "obter unico equipo" do
    before(:all) do
      payload = {
        thumbnail: Helpers::get_thumb("sanfona.jpg"),
        name: "Sanfona",
        category: "Outros",
        price: 119,
      }
      MongoDB.new.remove_equipo(payload[:name], @user_id)
      @equipo = Equipos.new.create(payload, @user_id)
      @equipo_id = @equipo.parsed_response["_id"]
      @result = Equipos.new.find_by_id(@equipo_id, @user_id)
    end
    it "Valida retorno 200" do
      expect(@result.code).to eql 200
    end
  end

  context "equipo nao existe" do
    before(:all) do
      @result = Equipos.new.find_by_id(MongoDB.new.get_mongo_id, @user_id)
    end
    it "Valida code 404" do
      expect(@result.code).to eql 404
    end
  end
end

describe "GET /equipos" do
  before(:all) do
    payload = { email: "robson@gmail.com", password: "123456" }
    result = Sessions.new.login(payload)
    @user_id = result.parsed_response["_id"]
  end

  context "obter uma lista de equipamentos" do
    before(:all) do
      payloads = [
        {
          thumbnail: Helpers::get_thumb("sanfona.jpg"),
          name: "Sanfona",
          category: "Outros",
          price: 119,
        },
        {
          thumbnail: Helpers::get_thumb("slash.jpg"),
          name: "Les Paul Slash",
          category: "Outros",
          price: 245,
        },
        {
          thumbnail: Helpers::get_thumb("trompete.jpg"),
          name: "Trompete",
          category: "Outros",
          price: 343,
        },
      ]
      payloads.each do |payload|
        MongoDB.new.remove_equipo(payload[:name], @user_id)
        Equipos.new.create(payload, @user_id)
      end
      @result = Equipos.new.list(@user_id)
    end
    it "Validar code 200" do
      expect(@result.code).to eql 200
    end
    it "Validar lista de equipos" do
      expect(@result.parsed_response).not_to be_empty
    end
  end
end
